<?php

namespace FEL\SistemaBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use FEL\SistemaBundle\Clase\RutasClass;
use FEL\SistemaBundle\Entity\correoIntercambio;

set_time_limit(0);
//ini_set('memory_limit', '7500M');


class ActualizaCorreoIntercambioCommand extends ContainerAwareCommand {

    protected function configure() {
        $this->setName('funcion:ActualizaCorreoIntercambio')
                ->setDescription('Ejecuta una al dia Actualiza lista de Correos de intercambio de Contribuyentes.')
//                ->addArgument('idEmpresa', InputArgument::OPTIONAL, 'Sin parametros')
        ;
    }
    private $lsPath = "/var/www/sistema.dtefacturaenlinea.cl/web/correoIntercambio/";
    private $lsPathProcesados = "/var/www/sistema.dtefacturaenlinea.cl/web/correoIntercambio/Procesados/";
    private $em = "";

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->em = $this->getContainer()->get('doctrine')->getManager("fel");

        $parametro = array('em' => $this->em);
        $this->borrarArchivos();
        $this->obtenerArchivo($this->em);
        $this->actualizaCorreoIntercambio($parametro);
    }

    public function obtenerArchivo($em){
        $fecha = date('Ymd');
        $nombreArchivo = "GeneraAutoma_".$fecha;
        $nombreArchivoDividido = $nombreArchivo.'_*';
        //$clave = $this->obtenerClave($em);
        $cookies = $this->obtenerCookie();
        /*$clave = $cookies['TOKEN'];
        $rutCompleto = '13458241-3';
        $separarRut = explode('-',$rutCompleto);
        $rut = $separarRut[0];
        $dv = $separarRut[1];        
        $fechaExpira = $cookies['TOKEN'];*/

        /*$cookie = "";
        foreach ($cookies as $key => $cok) {
            $cookie.= $key."=".$cok."; ";
        }
        //echo $cookie;*/
        //die();
        //echo $this->lsPath.$nombreArchivo."_0.csv";
        if( file_exists($this->lsPath.$nombreArchivo."_0.csv")||
            file_exists($this->lsPath.$nombreArchivo."_1.csv")||
            file_exists($this->lsPath.$nombreArchivo."_2.csv")||
            file_exists($this->lsPath.$nombreArchivo."_3.csv")||
            file_exists($this->lsPath.$nombreArchivo."_4.csv")||
            file_exists($this->lsPath.$nombreArchivo."_5.csv")||
            file_exists($this->lsPath.$nombreArchivo."_6.csv")||
            file_exists($this->lsPath.$nombreArchivo."_7.csv")||
            file_exists($this->lsPath.$nombreArchivo."_8.csv")||
            file_exists($this->lsPath.$nombreArchivo."_9.csv")||
            file_exists($this->lsPath.$nombreArchivo."_10.csv")||
            file_exists($this->lsPath.$nombreArchivo."_11.csv")||
            file_exists($this->lsPath.$nombreArchivo."_12.csv")||
            file_exists($this->lsPath.$nombreArchivo."_13.csv")||
            file_exists($this->lsPath.$nombreArchivo."_14.csv")||
            file_exists($this->lsPath.$nombreArchivo."_15.csv")||
            file_exists($this->lsPath.$nombreArchivo."_16.csv")||
            file_exists($this->lsPath.$nombreArchivo."_17.csv")||
            file_exists($this->lsPath.$nombreArchivo."_18.csv")){
            return false;
        }
        //die();
        foreach (glob($this->lsPathProcesados.$nombreArchivoDividido.".csv") as $nombre_fichero) {
            echo "Tamano de $nombre_fichero " . filesize($nombre_fichero) . "\n";
            return false;
        }
        //certificado
        
        echo "Descargando Archivo";

        $head[] = "Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
        $head[] = "Accept-Encoding:gzip, deflate, br";
        $head[] = "Accept-Language:es-ES,es;q=0.9";
        $head[] = "Connection:keep-alive";
       // $head[] = "Cookie:NETSCAPE_LIVEWIRE.rut=16085814; NETSCAPE_LIVEWIRE.rutm=16085814; NETSCAPE_LIVEWIRE.dv=1; NETSCAPE_LIVEWIRE.dvm=1; NETSCAPE_LIVEWIRE.clave=SImsv7bHXsUn2SIVrgB.vJmLUU; NETSCAPE_LIVEWIRE.mac=40t6a4lss8boelpl2kiih7v725; NETSCAPE_LIVEWIRE.exp=20181121231434; NETSCAPE_LIVEWIRE.sec=0000; NETSCAPE_LIVEWIRE.lms=120; TOKEN=XFoQuoN3N0Wxs; CSESSIONID=XFoQuoN3N0Wxs; RUT_NS=16085814; DV_NS=1; NETSCAPE_LIVEWIRE.locexp=Thu%2C%2022%20Nov%202018%2002%3A14%3A46%20GMT; EMG=16085814; cert_Origin=directo";
        //$head[] = "Cookie: cert_Origin=hercules.sii.cl; NETSCAPE_LIVEWIRE.rut=13458241; NETSCAPE_LIVEWIRE.rutm=13458241; NETSCAPE_LIVEWIRE.dv=3; NETSCAPE_LIVEWIRE.dvm=3; NETSCAPE_LIVEWIRE.clave=SIGWL93Zm52JASIYD/Os3ErIPc; NETSCAPE_LIVEWIRE.mac=7cr654ptlcf9at0fjubbfe0bdv; NETSCAPE_LIVEWIRE.exp=20181122142924; NETSCAPE_LIVEWIRE.sec=0000; NETSCAPE_LIVEWIRE.lms=120; TOKEN=ZB10rvaekQxN6; CSESSIONID=ZB10rvaekQxN6; RUT_NS=13458241; DV_NS=3; NETSCAPE_LIVEWIRE.locexp=Thu%2C%2022%20Nov%202018%2017%3A29%3A50%20GMT";
        //$head[] = "Cookie:NETSCAPE_LIVEWIRE.rut=".$rut."; NETSCAPE_LIVEWIRE.rutm=".$rut."; NETSCAPE_LIVEWIRE.dv=".$dv."; NETSCAPE_LIVEWIRE.dvm=".$dv."; NETSCAPE_LIVEWIRE.exp=".$fechaCompleta."; NETSCAPE_LIVEWIRE.sec=0000; NETSCAPE_LIVEWIRE.lms=120; TOKEN=".$clave."; CSESSIONID=".$clave."; RUT_NS=".$rut."; DV_NS=".$dv."; EMG=".$rut. "; cert_Origin=hercules.sii.cl";
        $head[] = "Cookie:".$cookies."cert_Origin=hercules.sii.cl";

        //var_dump($head);
        //die();

        $ch = curl_init(); 
        curl_setopt($ch,CURLOPT_URL,"https://palena.sii.cl/cvc_cgi/dte/ce_empresas_dwnld");
        curl_setopt($ch,CURLOPT_USERAGENT,"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"); 
        curl_setopt($ch,CURLOPT_HTTPHEADER,$head);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch,CURLOPT_TIMEOUT, 30); 
        curl_setopt($ch,CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);

        //Guardar pagina 
        $result = curl_exec($ch);
        if(curl_errno($ch)) // check for execution errors
        {
            echo 'Scraper error: ' . curl_error($ch);
            exit;
        }
        if(strlen($result)<5000){
            //$p['fileAdjunto']=$fileLogError;
            $p['asunto']="Alerta Descarga Archivo Correos de Intercambio FW";
            $p['body']="<p> Ocurrió un problema al descargar el CSV para actualizar los correos de intercambio </p>";
            $p['correo']=array(
                'felipe.vasquez@facturaenlinea.cl' => "Felipe Vásquez" );
            $this->enviarAlertaCorreo($p);
        }

        file_put_contents($this->lsPath.$nombreArchivo.".txt",$result);
        rename($this->lsPath.$nombreArchivo.".txt", $this->lsPath.$nombreArchivo.".csv");
        curl_close($ch);
        $this->dividirArchivo($nombreArchivo.".csv");
        unlink($this->lsPath.$nombreArchivo.".csv");
        //die();
    }

    public function actualizaCorreoIntercambio($parametro) {
        $em = $parametro['em'];
//        echo " 1 ";
//        $lsPath = RutasClass::getRutaCorreoIntercambio();
        //$lsPath = "/var/www/sistema.dtefacturaenlinea.cl/web/correoIntercambio/";
        //$lsPathProcesados = "/var/www/sistema.dtefacturaenlinea.cl/web/correoIntercambio/Procesados/";
        //$lsPath = "/var/www/qa.dtefacturaenlinea.cl/web/correoIntercambio/";
//        $lsPath = "correoIntercambio/";
//        $fileLog = fopen($lsPath."zlogError.txt", "w");
//        echo " 2 ";
        $fileLogProceso = fopen($this->lsPath . "logPreceso.log", "a+");
//        echo " 3 ";
        $fileLogError = fopen($this->lsPath . "logError.log", "a+");
//        echo " 4 ";
//        echo "\$this->lsPath $this->lsPath\n";
        $lsDirectorio = opendir($this->lsPath);

        $archivoExtencion="";
        $existeError=false;
        $procesaArchivo=false;
//        echo "openDir:".$lsDirectorio."\n";
        while ($lsArchivo = readdir($lsDirectorio)) {
        //foreach (glob($this->lsPath."*.csv",GLOB_NOSORT) as $lsArchivo) {
//            echo "\n ::".$this->lsPath.$lsArchivo;
//            echo " arr ".$this->lsPath.$lsArchivo ."\n";
            if (!is_dir($this->lsPath . $lsArchivo)) {
                $arrArchivoExtension = explode(".", $lsArchivo);
                $archivoExtencion = $arrArchivoExtension[1];
                
                $ruta = $this->lsPath . $lsArchivo;
                $rutaProcesados = $this->lsPathProcesados. $lsArchivo;
                
                if ($archivoExtencion=="csv") {
                    $liNumeroLinea = 0;
                    $contActualiza = 0;
                    $contClear = 0;
                    $proximoEcho = 1;
                    fwrite($fileLogProceso, "Procesado [" . date("Y-m-d H:i:s") . "] -> " . $lsArchivo . "" . PHP_EOL);
                    if ($file = fopen($ruta, 'r')) {
                        while (!feof($file)) {
                            $liNumeroLinea++;
                            $contActualiza++;
                            $contClear++;
                            $linea = trim(fgets($file));
                            $cue = strlen(trim($linea));
                            $datos = explode(";", $linea);
                            $this->flush_buffers();
                            if ($cue > 0 && $liNumeroLinea >= 2) {
                                try {
                                    if (array_key_exists('0', $datos)) {
                                        $arrContribuyenteRut = explode("-", trim($datos[0]));
                                        if (count($arrContribuyenteRut) == 2) {
                                            $contribuyenteRut = ($arrContribuyenteRut[0] * 1) . "-" . $arrContribuyenteRut[1];
                                        } else {
                                            $contribuyenteRut = '';
                                        }
                                    } else {
                                        $arrContribuyenteRut = '';
                                        $contribuyenteRut = '';
                                    }
                                    /*
                                    if (array_key_exists('1', $datos)) {
                                        $contribuyenteRazonSocial = trim(utf8_encode($datos[1]));
                                    } else {
                                        $contribuyenteRazonSocial = '';
                                    }
                                       
                                    $keyNumero = 2;
                                    if (array_key_exists($keyNumero, $datos)) {
                                        for ($i=$keyNumero; $i < sizeof($datos) ; $i++) { 
                                            if(filter_var(trim($datos[$i]), FILTER_VALIDATE_INT)){
                                                break;
                                            }
                                        }
                                        $keyNumero=$i;
                                        if (array_key_exists($keyNumero, $datos))
                                            $contribuyenteResolucionNumero = $datos[$keyNumero];
                                        else
                                            $contribuyenteResolucionNumero = '';
                                    } else {
                                        $contribuyenteResolucionNumero = '';
                                    }
                                                                       
                                    $keyFecha=$keyNumero+1;
                                    if (array_key_exists($keyFecha, $datos)) {
                                        $resolucionFecha = explode("-", trim($datos[$keyFecha]));

                                        if (count($resolucionFecha) == 3) {
                                            $contribuyenteResolucionFecha = ( $resolucionFecha[2] . "-" . $resolucionFecha[1] . "-" . $resolucionFecha[0] );
                                        } else {
                                            $contribuyenteResolucionFecha = '';
                                        }
                                    } else {
                                        $resolucionFecha = '';
                                        $contribuyenteResolucionFecha = '';
                                    }
                                    */
                                    $keyEmail=4;
                                    if (array_key_exists($keyEmail, $datos)) {
                                        
                                        for ($i=$keyEmail; $i < sizeof($datos) ; $i++) { 
                                            //if(filter_var(trim($datos[$i]), FILTER_VALIDATE_EMAIL)){
                                            if(strpos($datos[$i],'@')){
                                                break;
                                            }
                                        }
                                        $keyEmail=$i;
                                        if (array_key_exists($keyEmail, $datos))
                                            $contribuyenteCorreoIntercambio = $datos[$keyEmail];
                                        else
                                            $contribuyenteCorreoIntercambio = '';
                                    } else {
                                        $contribuyenteCorreoIntercambio = '';
                                    }
                                /* $keyWeb = sizeof($datos)-1;
                                    if (array_key_exists($keyWeb, $datos)) {
                                        $contribuyenteWeb = ( $datos[$keyWeb]);
                                    } else {*/
                                        $contribuyenteWeb = '';
                                    //}
                                    if(empty($contribuyenteCorreoIntercambio)){
                                        echo "ERROR Fila: $liNumeroLinea.[$contribuyenteRut][$contribuyenteCorreoIntercambio]\n";
                                        goto ERROR;
                                    }
                                } catch (Exception $ex) {
                                    goto ERROR;
                                }
                                echo "Fila: $liNumeroLinea.[$contribuyenteRut][$contribuyenteCorreoIntercambio]\n";
                                if ($contribuyenteRut != '' && $contribuyenteCorreoIntercambio != ''
                                ) {

                                    $objCorreoIntercambio = $em->getRepository("FELSistemaBundle:correoIntercambio")->findOneBy(array('correoIntercambioRut' => $contribuyenteRut));
                                    if (count($objCorreoIntercambio) > 0) {
                                        $contribuyenteEmail = $objCorreoIntercambio->getCorreoIntercambioEmail();
                                        if (trim($contribuyenteCorreoIntercambio) != trim($contribuyenteEmail)) {
                                            //$objCorreoIntercambio->setCorreoIntercambioRazonSocial("");
                                            //$objCorreoIntercambio->setCorreoIntercambioResolucionNumero($contribuyenteResolucionNumero);
                                            //$objCorreoIntercambio->setCorreoIntercambioResolucionFecha(date_create($contribuyenteResolucionFecha));
                                            $objCorreoIntercambio->setCorreoIntercambioEmail($contribuyenteCorreoIntercambio);
                                            
                                            $em->persist($objCorreoIntercambio);
                                            if ($contActualiza >= 100) {
                                                $contActualiza = 0;
                                                $em->flush();
                                                //$em->clear();
                                                //$this->flush_buffers();
                                            }
                                        } 
                                    } else {
                                        $objCorreoIntercambio = new correoIntercambio();
                                        $objCorreoIntercambio->setCorreoIntercambioRut($contribuyenteRut);
                                        $objCorreoIntercambio->setCorreoIntercambioRazonSocial('');
                                        $objCorreoIntercambio->setCorreoIntercambioResolucionNumero('');
                                        //$objCorreoIntercambio->setCorreoIntercambioResolucionFecha(date_create($contribuyenteResolucionFecha));
                                        $objCorreoIntercambio->setCorreoIntercambioEmail($contribuyenteCorreoIntercambio);
                                        $em->persist($objCorreoIntercambio);
                                        if ($contActualiza >= 100) {
                                            $contActualiza = 0;
                                            $em->flush();
                                            //$em->clear();
                                            //$this->flush_buffers();
                                        }
                                    }
                                    if($contClear >= 10000){
                                        $contClear = 0;
                                        $em->flush();
                                        $em->clear();
                                    }                                    
                                    $procesaArchivo=true;
                                } else {
                                    ERROR:
                                    fwrite($fileLogError, date("Y-m-d H:i:s ") . " Archivo[" . $lsArchivo . "] Linea[" . $linea . "]\n" . PHP_EOL);
                                    $existeError=true;
                                }
                            }
                        }
                        $em->flush();
                        $em->clear();
                        fclose($file);

                        try {
                            if (!copy($ruta, $rutaProcesados)){
                                echo "Error al copiar $rutaProcesados \n";
                            }
                            unlink($ruta);
                        } catch (\Exception $e) {
                            echo "Excepcion al eliminar archivo de correo intercambio: $e";
                        }
                    }
                } else {
                    echo "No existe archivo CSV\n";
                }
            }
            else {
                echo "No existe archivo\n";
            }
            $contActualiza = 0;
            $contClear = 0;
            $em->flush();
            $em->clear();
        }
        
        fclose($fileLogProceso);
        fclose($fileLogError);

        if(!empty($existeError)){
            $p['fileAdjunto']=$fileLogError;
            $p['asunto']="Alerta Descarga Archivo Correos de Intercambio";
            $p['body']="<p> Ocurrió un problema al descargar el CSV para actualizar los correos de intercambio </p>";
            $p['correo']=array(
                'felipe.vasquez@facturaenlinea.cl' => "Felipe Vásquez" );
            $this->enviarAlertaCorreo($p);
        }
        if($procesaArchivo){
            $this->verificarCorreoCliente();
        }
    }
    private function verificarCorreoCliente(){
        $em = $this->em;
        $sql = "SELECT * FROM empresaEntidad e 
                INNER JOIN correoIntercambio c ON c.correoIntercambioRut=e.empresaEntidadRut 
                WHERE e.id_empresa=13 AND id_empresaEntidadTipo=1 AND e.empresaEntidadBaja=0";
        $query = $em->getConnection()->prepare($sql);
        $query->execute();

        $empresas = $query->fetchAll();
        $arrEmp=[];
        if(count($empresas)>0){
            foreach ($empresas as $emp) {
                $correoInter = $emp['correoIntercambioEmail'];
                if (strlen(stristr($correoInter,'dtefacturaenlinea'))==0) {
                    $arrEmp[$emp['id']]['razonSocial'] = $emp['correoIntercambioRazonSocial'];
                    $arrEmp[$emp['id']]['rut'] = $emp['correoIntercambioRut'];
                    $arrEmp[$emp['id']]['correo'] = $correoInter;
                }
            }
            if(count($arrEmp)>0){
                //$p['fileAdjunto']=$fileLogError;
                $p['asunto']="Alerta Clientes con Correo de Intercambio Cambiado";
                $body="<p>Se ha detectado un cambio en el correo de intercambio de algunas empresas:</p>";
                foreach ($arrEmp as $key => $value) {                    
                    $body.="<p>".$value['razonSocial']."(".$value['rut'].") - ".$value['correo']."</p>";
                }
                $p['body']=$body;
                $p['correo']=array(
                    'felipe.vasquez@facturaenlinea.cl' => "Felipe Vásquez",
                    'cristian.ibanez@facturaenlinea.cl' => "Cristian Ibañez",
                    'pagos@facturaenlinea.cl' => "Pagos Factura en Linea",
                    'camila.lufi@facturaenlinea.cl' => "Camila Lufi" );
                $this->enviarAlertaCorreo($p);
            }
        }
    }
    private function obtenerCookie(){
        
        //certificado
        $rutaCert = "/var/www/certificado/";
        $filePass = "facturaenlinea13";
        $fileCrt = $rutaCert."file.crt.pem";
        $fileKey = $rutaCert."file.key.pem";

        $head[] = "Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
        $head[] = "Accept-Encoding:gzip, deflate, br";
        $head[] = "Accept-Language:es-ES,es;q=0.9";
        $head[] = "Connection:keep-alive";

        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, "https://hercules.sii.cl/cgi_AUT2000/autInicio.cgi?referencia=https://palena.sii.cl/cvc_cgi/dte/ce_consulta_rut");
        curl_setopt($ch,CURLOPT_USERAGENT,"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"); 
        curl_setopt($ch,CURLOPT_HTTPHEADER,$head);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch,CURLOPT_TIMEOUT, 10); 
        curl_setopt($ch,CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, true);

        //certificado
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt($ch, CURLOPT_SSLCERT, $fileCrt);
        curl_setopt($ch, CURLOPT_SSLKEY, $fileKey);
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $filePass);
        curl_setopt($ch, CURLOPT_SSLKEYPASSWD, $filePass);

        curl_setopt($ch, CURLOPT_HEADER, 1);

        //Guardar pagina 
        $result = curl_exec($ch);
        if(curl_errno($ch)) // check for execution errors
        {
            echo 'Scraper error: ' . curl_error($ch);
            exit;
        }
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);
        //$cookies = array();
        $cookies = '';
        foreach($matches[1] as $item) {
            //echo "item: ".$item."\n";
            /*parse_str($item, $cookie);
            $cookies = array_merge($cookies, $cookie);*/
            $cookies.=$item."; ";
        }
        //var_dump($cookies);
        return $cookies;
    }
    public function obtenerClave($emFEL){
        $rutaCertificado = \FEL\SistemaBundle\Clase\RutasClass::getRutaCertificado(array('empresaRut'=>'76102645-3'));
        $empresaCertificado = $emFEL->getRepository("FELSistemaBundle:empresaCertificado")->findOneBy(array('idEmpresa'=>13,'empresaCertificadoVigente'=>'1'),array("id" => 'DESC'));
        
        $empresaCertificadoNombre= $rutaCertificado.$empresaCertificado->getEmpresaCertificadoArchivo();
        $empresaCertificadoClave = $empresaCertificado->getEmpresaCertificadoClave();

        $config = [
            'firma' => [
                'file' => $empresaCertificadoNombre,
                //'data' => '', // contenido del archivo certificado.p12
                'pass' => $empresaCertificadoClave,
            ],
        ];
        //var_dump($config);

        $reintentos=0;
        do {
            $token = \FEL\SistemaBundle\Sii\SiiClass::getToken($config['firma']);
            $reintentos++;
            if($token){
                $reintentos=5;
            }
        } while ($reintentos<5);

        return $token;
    }

    private function enviarAlertaCorreo($p){
        $asunto=$p['asunto'];
        $body=$p['body'];
        $fileAdjunto=(empty($p['fileAdjunto']))?'':$p['fileAdjunto'];
        $correo=$p['correo'];

        $transport = \Swift_SmtpTransport::newInstance('mail.dtefacturaenlinea.cl', 25)
        ->setUsername("alertas@dtefacturaenlinea.cl")
        ->setPassword('12alertas');

        $mailer = \Swift_Mailer::newInstance($transport);
        $mailLogger = new \Swift_Plugins_Loggers_ArrayLogger();
        //$asunto = "Alerta Descarga Archivo Correos de Intercambio";

        //$body = "<p> Ocurrió un problema al descargar el CSV para actualizar los correos de intercambio </p>";
        $mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($mailLogger));
        
        $message = \Swift_Message::newInstance();

        $message->setSubject($asunto);
        $message->setFrom(array('alertas@dtefacturaenlinea.cl'=> 'Alertas Facturaenlinea'));
        $message->setTo($correo);

        $message->setBody( $body , 'text/html' );
        $message->addPart( $asunto , 'text/plain' );

        if(!empty($fileAdjunto)){
            $message->attach(Swift_Attachment::fromPath($fileAdjunto));
        }

        if ($mailer->send($message)) {                
        }
        exit;

    }

    private function dividirArchivo($lsArchivo=''){
        /**
        * Split a CSV file
        *
        * Each row is its own line.
        * Each cell is comma-separated
        * This file splits it into piece of size $size, add the header row
        * and names the resulting file filename_X.csv where filename is the
        * name of the original file and X is an incrementing integer.
        */

        // Editable Options
        $size = 10000000; // about 10Mb
        
        //$lsPath = "/var/www/sistema.dtefacturaenlinea.cl/web/correoIntercambio/";
        //$lsPathProcesados = "/var/www/sistema.dtefacturaenlinea.cl/web/correoIntercambio/Procesados/";
        
        if(empty($lsArchivo)){
            while ($lsArchivo = readdir($lsDirectorio)) {
                if (!is_dir($this->lsPath . $lsArchivo)) {
                    $arrArchivoExtension = explode(".", $lsArchivo);
                    $archivoExtencion = $arrArchivoExtension[1];
                    
                    if ($archivoExtencion == "csv") {
                        break;
                    }
                }
            }
            $nombreArchivo = $arrArchivoExtension[0];
            $ruta = $this->lsPath . $lsArchivo;
            $rutaProcesados = $this->lsPathProcesados. $lsArchivo;
        }
        else{
            $arrArchivoExtension = explode(".", $lsArchivo);
            $nombreArchivo = $arrArchivoExtension[0];
            $ruta =   $this->lsPath.$lsArchivo;
            $rutaProcesados = $this->lsPathProcesados. $lsArchivo;
        }
        
        // Do not edit
        $done = false;
        $part = 0;
        if (($handle = fopen($ruta, "r")) !== FALSE) {
            $header = fgets($handle);
            while ($done == false) {
                $locA = ftell($handle); // gets the current location. START
                fseek($handle, $size, SEEK_CUR); // jump the length of $size from current position
                $tmp = fgets($handle); // read to the end of line. We want full lines
                $locB = ftell($handle); // gets the current location. END
                $span = ($locB - $locA);
                fseek($handle, $locA, SEEK_SET); // jump to the START of this chunk
                $chunk = fread($handle,$span); // read the chunk between START and END
                file_put_contents($this->lsPath.$nombreArchivo.'_'.$part.'.csv',$header.$chunk);
                $part++;
                if (strlen($chunk) < $size) $done = true;
            }
            fclose($handle);
        }
    }

    private function borrarArchivos(){
        $dir = opendir($this->lsPathProcesados);
        while($f = readdir($dir))
        {        
        if((time()-filemtime($this->lsPathProcesados.$f) > 3600*24*14) and !(is_dir($this->lsPathProcesados.$f)))//borrar archivos anterior a 14 dias
        unlink($this->lsPathProcesados.$f);
        }
        closedir($dir);
    }

    private function flush_buffers(){
        if (ob_get_contents()) {
            ob_end_flush();
            ob_flush();
        }
        /*flush();
        ob_start();*/
    }

// Fin Function
}

// Fin Class
